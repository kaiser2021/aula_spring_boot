package com.projeto.user.services;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projeto.user.domain.Categoria;
import com.projeto.user.domain.Produto;
import com.projeto.user.repositories.CategoriaRepositoy;
import com.projeto.user.repositories.ProdutoRepository;

@Service
public class CategoriaService {
	
	// operação para buscar a categoria por código
	
	// chamar a camada repository
	
	@Autowired
	private CategoriaRepositoy categoriaRepositoy;
	
	private ProdutoRepository produtoRepository;
	
	
	// buscar categoria por id
	public Optional<Categoria> buscar(Integer id) {
		
	Optional<Categoria> objCategoria=categoriaRepositoy.findById(id);
		
		
		
		
		return objCategoria;
		
		
		
	}
	
	
	public void cadastrar() {
		// instanciando Categorias
		Categoria cat1=new Categoria(null,"informática");
		Categoria cat2=new  Categoria(null,"Escritório");
		// instanciando produtos
		
		Produto p1=new Produto(null,"Computador",2000.00);
		Produto p2=new Produto(null,"Impressora",800.00);
		Produto p3=new Produto(null,"Mouse",80.00);
		
		//adicionar produtos dentro de cada Categorias
		
		cat1.getProdutos().addAll(Arrays.asList(p1,p2,p3));
		cat2.getProdutos().addAll(Arrays.asList(p2));
		
		//adicionar categorias de cada produto
		
		p1.getCategorias().addAll(Arrays.asList(cat1));
		p2.getCategorias().addAll(Arrays.asList(cat1,cat2));
		p3.getCategorias().addAll(Arrays.asList(cat1));
		
		categoriaRepositoy.saveAll(Arrays.asList(cat1,cat2));
		produtoRepository.saveAll(Arrays.asList(p1,p2,p3));
		
		
		
		
		
		
	}
	
	
	
	

}
