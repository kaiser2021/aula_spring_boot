package com.projeto.user;


import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.projeto.user.domain.Categoria;
import com.projeto.user.domain.Produto;
import com.projeto.user.repositories.CategoriaRepositoy;
import com.projeto.user.repositories.ProdutoRepository;



@SpringBootApplication
public class ProjetoUser1Application  implements CommandLineRunner{
	
	

	@Autowired
	private CategoriaRepositoy categoriaRepositoy;
	@Autowired
	private ProdutoRepository produtoRepository;
	
	
	

	public static void main(String[] args) {
		SpringApplication.run(ProjetoUser1Application.class, args);
		
		
		
		
		
	}

	@Override
	public void run(String... args) throws Exception {
		
		
		// instanciando Categorias
				Categoria cat1=new Categoria(null,"informática");
				Categoria cat2=new  Categoria(null,"Escritório");
				// instanciando produtos
				
				Produto p1=new Produto(null,"Computador",2000.00);
				Produto p2=new Produto(null,"Impressora",800.00);
				Produto p3=new Produto(null,"Mouse",80.00);
				
				//adicionar produtos dentro de cada Categorias
				
				cat1.getProdutos().addAll(Arrays.asList(p1,p2,p3));
				cat2.getProdutos().addAll(Arrays.asList(p2));
				
				//adicionar categorias de cada produto
				
				p1.getCategorias().addAll(Arrays.asList(cat1));
				p2.getCategorias().addAll(Arrays.asList(cat1,cat2));
				p3.getCategorias().addAll(Arrays.asList(cat1));
				
				categoriaRepositoy.saveAll(Arrays.asList(cat1,cat2));
				produtoRepository.saveAll(Arrays.asList(p1,p2,p3));
				
		
		
		
		
		
		
	}

	
	
	
	
}
